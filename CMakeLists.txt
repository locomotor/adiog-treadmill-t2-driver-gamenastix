cmake_minimum_required(VERSION 3.4)
project(treadmill-t2-driver-gamenastix)
enable_testing()

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/openvr/headers)

file(GLOB_RECURSE sources ${CMAKE_CURRENT_SOURCE_DIR}/**.cpp)
file(GLOB_RECURSE headers ${CMAKE_CURRENT_SOURCE_DIR}/**.h)

add_library(treadmill-driver ${sources} ${headers})



