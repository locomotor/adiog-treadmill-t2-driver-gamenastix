// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#include "DriverFactory.h"


HMD_DLL_EXPORT void *HmdDriverFactory(const char *pInterfaceName, int *pReturnCode)
{
    if (0 == _stricmp(vr::IServerTrackedDeviceProvider_Version, pInterfaceName))
    {
        return &server_driver;
    }

    if (0 == _stricmp(vr::IVRWatchdogProvider_Version, pInterfaceName))
    {
        return &watchdog_driver;
    }

    if (pReturnCode)
    {
        *pReturnCode = vr::VRInitError_Init_InterfaceNotFound;
    }

    return nullptr;
}

ServerDriver server_driver;
WatchdogDriver watchdog_driver;
