// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once

#include "DiscoTracker.h"
#include "XBoxController.h"
#include <openvr_driver.h>
#include <algorithm>
#include <chrono>


class ServerDriver : public vr::IServerTrackedDeviceProvider
{
public:
    ServerDriver() = default;

    ~ServerDriver() = default;

    vr::EVRInitError Init(vr::IVRDriverContext *driverContext) override;

	static vr::HmdQuaternion_t ConvertEulerAnglesToQuaternion(double yawDegrees, double pitchDegrees, double rollDegrees);

    void Cleanup() override;

    const char *const *GetInterfaceVersions() override;

    void RunFrame() override;

    bool ShouldBlockStandbyMode() override;

    void EnterStandby() override;

    void LeaveStandby() override;

private:
    void ServerDriver::SetTracker(vr::DriverPose_t& driverPose, DiscoTracker& discoTracker, short padX, short padY, BYTE padZ);

    DiscoTracker trackerFootLeft;
    DiscoTracker trackerFootRight;
};
