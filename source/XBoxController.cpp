// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#include "XBoxController.h"


XBoxController::XBoxController(int playerNumber)
{
    controllerIndex = playerNumber - 1;
}

void XBoxController::ResetInternalState()
{
    ZeroMemory(&xInputState, sizeof(XINPUT_STATE));
}

DWORD XBoxController::GetInternalState()
{
    return XInputGetState(controllerIndex, &xInputState);
}

XINPUT_STATE XBoxController::GetState()
{
    ResetInternalState();
    (void) GetInternalState();
    return xInputState;
}

bool XBoxController::IsConnected()
{
    ResetInternalState();
    DWORD result = GetInternalState();
    return result == ERROR_SUCCESS;
}