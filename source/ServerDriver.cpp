// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#include "ServerDriver.h"
#include <cmath>
#include "glm/glm.hpp"


vr::EVRInitError ServerDriver::Init(vr::IVRDriverContext* driverContext)
{
    VR_INIT_SERVER_DRIVER_CONTEXT(driverContext);

    vr::DriverPose_t testPose = {0};
    testPose.deviceIsConnected = true;
    testPose.poseIsValid = true;
    testPose.willDriftInYaw = false;
    testPose.shouldApplyHeadModel = false;
    testPose.poseTimeOffset = 0;
    testPose.result = vr::ETrackingResult::TrackingResult_Running_OK;
    testPose.qDriverFromHeadRotation = {1, 0, 0, 0};
    testPose.qWorldFromDriverRotation = {1, 0, 0, 0};

    vr::VRControllerState_t testState;
    testState.ulButtonPressed = testState.ulButtonTouched = 0;

    trackerFootLeft = DiscoTracker("disco_tracker_foot_left", testPose, testState);
    trackerFootRight = DiscoTracker("disco_tracker_foot_right", testPose, testState);

    vr::VRServerDriverHost()->TrackedDeviceAdded("disco_tracker_foot_left", vr::TrackedDeviceClass_GenericTracker, &trackerFootLeft);
    vr::VRServerDriverHost()->TrackedDeviceAdded("disco_tracker_foot_right", vr::TrackedDeviceClass_GenericTracker, &trackerFootRight);

    return vr::EVRInitError::VRInitError_None;
}

void ServerDriver::Cleanup()
{
    // TODO
}

const char* const* ServerDriver::GetInterfaceVersions()
{
    return vr::k_InterfaceVersions;
}

namespace {
double scaleShort(short value, double rangeMeters)
{
    return static_cast<double>(value) / 32768.0 * rangeMeters;
}

double scaleByte(BYTE value, double rangeMeters)
{
    return static_cast<double>(value) / 256.0 * rangeMeters;
}
}

void ServerDriver::SetTracker(vr::DriverPose_t& driverPose, DiscoTracker& discoTracker, short padX, short padY, BYTE padZ)
{
    vr::DriverPose_t currentPose = discoTracker.GetPose();

    currentPose.vecPosition[0] = scaleShort(padX, 1.0);
    currentPose.vecPosition[1] = scaleByte(padZ, 0.5);
    currentPose.vecPosition[2] = -scaleShort(padY, 1.0);

    discoTracker.UpdateControllerPose(currentPose);
    vr::VRServerDriverHost()->TrackedDevicePoseUpdated(discoTracker.getObjectID(), discoTracker.GetPose(), sizeof(vr::DriverPose_t));

    driverPose = currentPose;
}

void ServerDriver::RunFrame()
{
    static vr::DriverPose_t leftPose = trackerFootLeft.GetPose();
    static vr::DriverPose_t rightPose = trackerFootRight.GetPose();
    
    static auto Gamepad = new XBoxController(1);

    static std::chrono::milliseconds lastMillis = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    std::chrono::milliseconds deltaTime = now - lastMillis;
    lastMillis = now;

    auto xInputGamepad = Gamepad->GetState().Gamepad;
    SetTracker(leftPose, trackerFootLeft, xInputGamepad.sThumbLX, xInputGamepad.sThumbLY, xInputGamepad.bLeftTrigger);
    SetTracker(rightPose, trackerFootRight, xInputGamepad.sThumbRX, xInputGamepad.sThumbRY, xInputGamepad.bRightTrigger);
}

bool ServerDriver::ShouldBlockStandbyMode()
{
    return false;
}

void ServerDriver::EnterStandby()
{
}

void ServerDriver::LeaveStandby()
{
}

vr::HmdQuaternion_t ServerDriver::ConvertEulerAnglesToQuaternion(double yawDegrees, double pitchDegrees, double rollDegrees)
{
    vr::HmdQuaternion_t quaternion;

    double cosYaw = cos(glm::radians(yawDegrees) * 0.5);
    double sinYaw = sin(glm::radians(yawDegrees) * 0.5);
    double cosRoll = cos(glm::radians(rollDegrees) * 0.5);
    double sinRoll = sin(glm::radians(rollDegrees) * 0.5);
    double cosPitch = cos(glm::radians(pitchDegrees) * 0.5);
    double sinPitch = sin(glm::radians(pitchDegrees) * 0.5);

    quaternion.w = cosYaw * cosRoll * cosPitch + sinYaw * sinRoll * sinPitch;
    quaternion.x = sinYaw * cosRoll * cosPitch - cosYaw * sinRoll * sinPitch;
    quaternion.y = cosYaw * cosRoll * sinPitch + sinYaw * sinRoll * cosPitch;
    quaternion.z = cosYaw * sinRoll * cosPitch - sinYaw * cosRoll * sinPitch;

    return quaternion;
}
