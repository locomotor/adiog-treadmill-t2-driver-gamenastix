// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#include "DiscoTracker.h"
#include <cstring>


DiscoTracker::DiscoTracker(std::string serialName, vr::DriverPose_t initialControllerPose, vr::VRControllerState_t initialControllerState)
        : serialName(std::move(serialName))
        , controllerPose(initialControllerPose)
        , controllerState(initialControllerState)
{
}

void DiscoTracker::UpdateControllerState(vr::VRControllerState_t newControllerState)
{
    controllerState = newControllerState;
}

void DiscoTracker::UpdateControllerPose(vr::DriverPose_t newControllerPose)
{
    controllerPose = newControllerPose;
}

uint32_t DiscoTracker::getObjectID()
{
    return objectId;
}

vr::VRControllerState_t DiscoTracker::GetControllerState()
{
    return controllerState;
}

bool DiscoTracker::TriggerHapticPulse(uint32_t unAxisId, uint16_t usPulseDurationMicroseconds)
{
    return false;
}

vr::EVRInitError DiscoTracker::Activate(uint32_t activatedObjectId)
{
    objectId = activatedObjectId;

    vr::PropertyContainerHandle_t prop_handle = vr::VRProperties()->TrackedDeviceToPropertyContainer(activatedObjectId);

    vr::VRProperties()->SetBoolProperty(prop_handle, vr::Prop_WillDriftInYaw_Bool, false);
    vr::VRProperties()->SetBoolProperty(prop_handle, vr::Prop_DeviceIsWireless_Bool, true);
    vr::VRProperties()->SetBoolProperty(prop_handle, vr::Prop_HasControllerComponent_Bool, true);

    vr::VRProperties()->SetInt32Property(prop_handle, vr::Prop_Axis0Type_Int32, vr::k_eControllerAxis_TrackPad);
    vr::VRProperties()->SetInt32Property(prop_handle, vr::Prop_Axis1Type_Int32, vr::k_eControllerAxis_Trigger);

    vr::VRProperties()->SetStringProperty(prop_handle, vr::Prop_SerialNumber_String, serialName.c_str());
    vr::VRProperties()->SetStringProperty(prop_handle, vr::Prop_ModelNumber_String, "Gamenastics Tracker");
    vr::VRProperties()->SetStringProperty(prop_handle, vr::Prop_RenderModelName_String, "vr_tracker_vive_1_0");
    vr::VRProperties()->SetStringProperty(prop_handle, vr::Prop_ManufacturerName_String, "DISCO WTMH S.A.");

    uint64_t available_buttons = vr::ButtonMaskFromId(vr::k_EButton_ApplicationMenu) | vr::ButtonMaskFromId(vr::k_EButton_SteamVR_Touchpad) | vr::ButtonMaskFromId(vr::k_EButton_SteamVR_Trigger) | vr::ButtonMaskFromId(vr::k_EButton_System) | vr::ButtonMaskFromId(vr::k_EButton_Grip);

    vr::VRProperties()->SetUint64Property(prop_handle, vr::Prop_SupportedButtons_Uint64, available_buttons);

    return vr::EVRInitError::VRInitError_None;
}

void DiscoTracker::Deactivate()
{
}

void DiscoTracker::EnterStandby()
{
}

void* DiscoTracker::GetComponent(const char* componentNameAndVersion)
{
    if (0 == strcmp(vr::IVRDriverInput_Version, componentNameAndVersion))
    {
        return this;
    }
    else
    {
        return nullptr;
    }
}

void DiscoTracker::DebugRequest(const char* requestString, char* responseBuffer, uint32_t responseBufferSize)
{
    if (responseBufferSize >= 1)
    {
        responseBuffer[0] = 0;
    }
}

vr::DriverPose_t DiscoTracker::GetPose()
{
    return controllerPose;
}
